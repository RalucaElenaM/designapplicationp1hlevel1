#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		//QObject::connect(okButton, SIGNAL(released()), this, SLOT(mf_OkButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setGeometry(QRect(50, 35, 301, 51));
		std::string defaultLabelHelloWorld = boost::any_cast<std::string>(m_TransientDataCollection["LabelHelloWorld"]);
		label->setText(QString(defaultLabelHelloWorld.c_str()));
		QFont font;
		font.setFamily(QStringLiteral("Calisto MT"));
		font.setPointSize(18);
		font.setBold(true);
		font.setWeight(75);
		label->setFont(font);
		m_uMainWindow->setCentralWidget(centralWidget);
		
	}	
}

