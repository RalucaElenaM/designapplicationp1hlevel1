#pragma once
#include <spring\Framework\IScene.h>

namespace Spring
{
	class BaseScene : public IScene
	{

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
		
	};

}
